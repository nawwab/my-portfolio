let nightflag = false;
const photo = document.getElementById("my-img");
const socmed = document.querySelectorAll("#button-wrap img");
const nightButton = document.getElementById("nightmode");

document.getElementById('nightmode').addEventListener('click', nightMode);

function nightMode() {
    console.log("bip blip");
    if (nightflag) {
        console.log("nightmode off");
        document.body.style.setProperty('--background-color', "#ffffff");
        document.body.style.setProperty('--font-color', "#232020");
        nightButton.src = "svg/icon night.svg";
        console.log(nightMode.src);

        socmed.forEach(element => {
            element.style.setProperty('filter', '');
        });

        photo.src = "photo.jpeg";
        photo.style.setProperty('filter', '');
        nightflag = false;
    } else if (!nightflag) {
        console.log("nightmode!!!.");
        document.body.style.setProperty('--background-color', "#232020");
        document.body.style.setProperty('--font-color', "#ffffff");
        nightButton.src = "svg/icon sun.svg";
        console.log(nightMode.src);

        socmed.forEach(element => {
            element.style.setProperty('filter', 'brightness(0) invert(1)');
        });

        photo.src = "photos.jpeg";
        photo.style.setProperty('filter', 'invert(1)')
        nightflag = true;
    }
}